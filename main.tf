# Based on the tutorial in 
# https://docs.aws.amazon.com/AmazonS3/latest/userguide/website-hosting-custom-domain-walkthrough.html

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}

# Set Account

resource "aws_iam_user" "eduardo_test_2" {
  name = "eduardo-test-2"
}

resource "aws_iam_group" "admin" {
  name = "second-admin-group"
}

resource "aws_iam_user_login_profile" "profile" {
  user    = aws_iam_user.eduardo_test_2.name
  password_reset_required = true
  pgp_key = "keybase:eduardo"
}

resource "aws_iam_policy" "policy" {
  name        = "test-policy"
  description = "A test policy"
  policy      = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "*"
        Effect = "Allow"
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_group_policy_attachment" "test-attach" {
  group      = aws_iam_group.admin.name
  policy_arn = aws_iam_policy.policy.arn
}

resource "aws_iam_group_membership" "team" {
  name = "tf-testing-group-membership"

  users = [
    aws_iam_user.eduardo_test_2.name,
  ]

  group = aws_iam_group.admin.name
}

output "password" {
  value = aws_iam_user_login_profile.profile.encrypted_password
}

# create user
# create group
# attach policy to group
# send invitation

